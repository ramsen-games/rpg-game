export default function NumberTransition(difference, callback, duration = 500) {
  return new Promise((resolve) => {
    let i = 1;
    const interval = setInterval(() => {
      const result = callback(interval);
      if (result) resolve(result);
      if (i >= difference) {
        resolve(false);
        clearInterval(interval);
      }
      i += 1;
    }, duration / difference);
  });
}
