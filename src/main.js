import { createApp } from 'vue';
import main from '@/components/main';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/styles/index.styl';

const app = createApp(App);

app
  .use(store)
  .use(router);

Object.values(main).forEach((component) => {
  app.component(component.name, component);
});

app.mount('#app');
