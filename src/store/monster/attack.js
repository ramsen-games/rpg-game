export default {
  state: {
    calculationDamage: {
      standart: 6,
      amplitude: 2,
    },
  },
  getters: {
    getCalculatedDamage: (state) => (random) => Math.floor(random * (state.calculationDamage.amplitude * 2 + 1))
      - state.calculationDamage.amplitude + state.calculationDamage.standart,
  },
  mutations: {
    REFRESH_CALCULATION(state) {
      state.calculationDamage.standart = 6;
      state.calculationDamage.amplitude = 2;
    },
  },
  actions: {
    refreshCalculationAndGetCalculatedDamage({ commit, getters }) {
      commit('REFRESH_CALCULATION');
      return getters.getCalculatedDamage(Math.random());
    },
  },
};
