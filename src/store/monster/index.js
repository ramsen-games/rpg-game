import attack from './attack';

export default {
  namespaced: true,
  state: {
    generation: {
      names: ['Snikrot', 'Ghazghkull', 'Zog'],
      minHP: 50,
      amplitudeOfHP: 25,
    },
  },
  getters: {
    generateMonster: (state) => (random) => {
      const nameIndex = Math.floor(Math.random() * 3);
      const hp = Math.floor(random * state.generation.amplitudeOfHP) + state.generation.minHP;

      return {
        name: state.generation.names[nameIndex],
        maxHP: hp,
        hp,
      };
    },
  },
  mutations: {
    REFRESH_GENERATION(state) {
      state.generation.names = ['Snikrot', 'Ghazghkull', 'Zog'];
      state.generation.minHP = 50;
      state.generation.amplitudeOfHP = 25;
    },
  },
  actions: {
    refreshGenerationAndGenerateMonster({ commit, getters }) {
      commit('REFRESH_GENERATION');
      return getters.generateMonster(Math.random());
    },
  },
  modules: {
    attack,
  },
};
