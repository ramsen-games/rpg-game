export default {
  state: {
    colors: {
      primary: '#b0e017',
      link: '#ff9f2f',
      info: '#8672ff',
      xp: '#43fffa',
      mana: '#1fffb2',
      hp: '#ff0081',
      damage: '#ff0010',
      white: '#f9f9f9',
      light: '#d8d8d8',
      gray: '#8e8e8e',
      black: '#2C311E',
    },
  },
  getters: {
    getColor: (state) => (color) => state.colors[color],
  },
  mutations: {
  },
  actions: {
  },
};
