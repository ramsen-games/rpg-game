import NumberTransition from '@/utils/NumberTransition';
// import attack from './attack';

export default {
  namespaced: true,
  state: {
    status: false,
    opponent: {},
    durationOfAttack: 1000,
  },
  getters: {
    isFight: (state) => state.status,
    getOpponent: (state) => state.opponent,
  },
  mutations: {
    ATTACK_OPPONENT(state, damage) {
      state.opponent.hp -= damage;
    },
  },
  actions: {
    async startFight({ state, dispatch }) {
      state.opponent = await dispatch('monster/refreshGenerationAndGenerateMonster', null, { root: true });
      state.status = true;
    },
    async attackOpponent({ state, commit, dispatch }) {
      const playerDamage = await dispatch('player/refreshCalculationAndGetCalculatedDamage', null, { root: true });
      const isOpponentBeated = await NumberTransition(playerDamage, (interval) => {
        if (state.opponent.hp > 0) commit('ATTACK_OPPONENT', 1);
        if (state.opponent.hp <= 0) {
          clearInterval(interval);
          return true;
        }
        return false;
      });

      if (isOpponentBeated) alert('Player win!');
      else dispatch('attackPlayer');
    },
    async attackPlayer({ rootGetters, commit, dispatch }) {
      let opponentDamage = await dispatch('monster/refreshCalculationAndGetCalculatedDamage', null, { root: true });
      opponentDamage = 25;
      const isPlayerBeated = await NumberTransition(opponentDamage, (interval) => {
        if (rootGetters['player/getInfo'].hp > 0) commit('player/ATTACK_PLAYER', 1, { root: true });
        if (rootGetters['player/getInfo'].hp <= 0) {
          clearInterval(interval);
          return true;
        }
        return false;
      });
      if (isPlayerBeated) alert('Opponent win!');
    },
  },
};
