export default {
  state: {
    regeneration: {
      percent: 15,
    },
  },
  getters: {
    getRegenerativeHP: (state) => (maxHP) => Math.round((maxHP / 100) * state.regeneration.percent),
  },
  mutations: {

  },
  actions: {

  },
};
