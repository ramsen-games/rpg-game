import NumberTransition from '@/utils/NumberTransition';
import hp from './hp';
import attack from './attack';

export default {
  namespaced: true,
  state: {
    params: {
      level: 1,
      hp: 50,
      maxHP: 100,
      xp: 0,
      maxXP: 125,
    },
  },
  getters: {
    getInfo: (state) => state.params,
  },
  mutations: {
    REGENERATE_HP(state, regenerativeHP) {
      NumberTransition(regenerativeHP, (interval) => {
        if (state.params.hp >= state.params.maxHP) clearInterval(interval);
        else state.params.hp += 1;
      });
    },
    ATTACK_PLAYER(state, damage) {
      state.params.hp -= damage;
    },
  },
  actions: {
    regenerateHP: ({ commit, state, getters }) => {
      commit('REGENERATE_HP', getters.getRegenerativeHP(state.params.maxHP));
    },
  },
  modules: {
    hp,
    attack,
  },
};
