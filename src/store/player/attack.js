export default {
  state: {
    calculationDamage: {
      standart: 10,
      amplitude: 2,
      trigger: 1,
    },
  },
  getters: {
    getCalculatedDamage: (state) => (random) => Math.floor(random * (state.calculationDamage.amplitude * 2 + 1))
      - state.calculationDamage.amplitude + state.calculationDamage.standart,
  },
  mutations: {
    REFRESH_CALCULATION(state) {
      state.calculationDamage.trigger += 1;
    },
  },
  actions: {
    refreshCalculationAndGetCalculatedDamage({ commit, getters }) {
      commit('REFRESH_CALCULATION');
      return getters.getCalculatedDamage(Math.random());
    },
  },
};
