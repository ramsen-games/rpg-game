import { createStore } from 'vuex';
import colors from './modules/colors';

import player from './player';
import monster from './monster';

import fight from './fight';

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    player,
    monster,
    fight,
    colors,
  },
});
